// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DeusExMachinaGameMode.generated.h"

UCLASS(minimalapi)
class ADeusExMachinaGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ADeusExMachinaGameMode();
};



