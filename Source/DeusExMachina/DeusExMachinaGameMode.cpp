// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "DeusExMachinaGameMode.h"
#include "DeusExMachinaHUD.h"
#include "DeusExMachinaCharacter.h"
#include "UObject/ConstructorHelpers.h"

ADeusExMachinaGameMode::ADeusExMachinaGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ADeusExMachinaHUD::StaticClass();
}
